package com.flink.examples.aggregate;

import com.flink.examples.DataSource;
import org.apache.flink.api.common.typeinfo.Types;
import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import java.util.List;

/**
 * @Description maxBy聚合：获取一组数据流算子中最小的记录行（和min的区别，min是返回计算字段的最小值）
 * @Author JL
 * @Date 2020/09/14
 * @Version V1.0
 */
public class MinBy {
    /**
     * 遍历集合，返回每个性别分区下最小年龄数据记录
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        List<Tuple3<String, String, Integer>> tuple3List = DataSource.getTuple3ToList();
        DataStream<Tuple3<String, String, Integer>> dataStream = env.fromCollection(tuple3List)
                .returns(Types.TUPLE(Types.STRING, Types.STRING,Types.INT))
                .keyBy((KeySelector<Tuple3<String, String, Integer>, String>) k ->k.f1)
                //按数量窗口滚动，每3个输入数据流，计算一次
                .countWindow(3)
                //注意：计算变量为f2
                .minBy(2);
        dataStream.print();
        env.execute("flink MinBy job");
    }
}
/*
4> (伍七,girl,18)
2> (张三,man,20)
 */