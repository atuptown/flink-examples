package com.flink.examples.functions;

import com.flink.examples.DataSource;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

import java.util.List;

/**
 * @Description Project算子：从数据流的元数组中，重新排例参数并指定不同的下标位，返回新的数据流
 * @Author JL
 * @Date 2020/09/17
 * @Version V1.0
 */
public class Project {

    /**
     * 官方文档：https://ci.apache.org/projects/flink/flink-docs-release-1.11/zh/dev/stream/operators/
     */

    /**
     * 在原有的数据源上重新排序组装一个新的数据源，并打印
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        List<Tuple3<String,String,Integer>> tuple3List = DataSource.getTuple3ToList();
        //project(索引下标，索引下标)，project参数对应数据流中Tuple3的下标位，可以指定多个下标位，返回一个新包含Tuple的数据流
        DataStream<Tuple2<Integer, String>> dataStream = env.fromCollection(tuple3List).project(2,0);
        dataStream.print();
        env.execute("flink Project job");
    }
}
/*
1> (29,王五)
3> (20,张三)
2> (32,刘六)
3> (18,伍七)
4> (24,李四)
4> (30,吴八)
*/
