package com.flink.examples.mysql;

import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.table.api.EnvironmentSettings;
import org.apache.flink.table.api.StatementSet;
import org.apache.flink.table.api.TableResult;
import org.apache.flink.table.api.bridge.java.StreamTableEnvironment;

/**
 * @Description 使用Tbale&SQL与Flink JDBC连接器将数据插入MYSQL数据库表
 * @Author JL
 * @Date 2021/01/15
 * @Version V1.0
 */
public class InsertToMysql {
    /**
     官方参考：https://ci.apache.org/projects/flink/flink-docs-release-1.11/dev/table/connectors/jdbc.html
     */
    //flink-jdbc-1.11.1写法,所有属性名在JdbcTableSourceSinkFactory工厂类中定义
    static String table_sql =
            "CREATE TABLE my_users (\n" +
            "  id BIGINT,\n" +
            "  name STRING,\n" +
            "  age INT,\n" +
            "  status INT,\n" +
            "  PRIMARY KEY (id) NOT ENFORCED\n" +
            ") WITH (\n" +
            "  'connector.type' = 'jdbc',\n" +
            "  'connector.url' = 'jdbc:mysql://127.0.0.1:3306/flink?useUnicode=true&characterEncoding=utf-8', \n" +
            "  'connector.driver' = 'com.mysql.jdbc.Driver', \n" +
            "  'connector.table' = 'users', \n" +
            "  'connector.username' = 'root',\n" +
            "  'connector.password' = '123456' \n" +
            ")";

    public static void main(String[] args) throws Exception {
        //构建StreamExecutionEnvironment
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        //构建EnvironmentSettings 并指定Blink Planner
        EnvironmentSettings bsSettings = EnvironmentSettings.newInstance().useBlinkPlanner().inStreamingMode().build();
        //构建StreamTableEnvironment
        StreamTableEnvironment tEnv = StreamTableEnvironment.create(env, bsSettings);
        //注册mysql数据维表
        tEnv.executeSql(table_sql);

        //执行SQL,id=0是因id字段为自增主键，为0则mysql识别会默认自增值代替
        String sql = "insert into my_users (id,name,age,status) values(0,'tom',31,0)";

        // 第一种方式：直接执行sql
//        TableResult tableResult = tEnv.executeSql(sql);

        //第二种方式：声明一个操作集合来执行sql
        StatementSet stmtSet = tEnv.createStatementSet();
        stmtSet.addInsertSql(sql);
        TableResult tableResult = stmtSet.execute();

        tableResult.print();
    }
}
