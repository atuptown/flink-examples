# flink-examples

#### 介绍
基于flink.1.11.1版本的工程示例，此示例包含大部份算子、窗口、中间件连接器、tables&sql的用法，适合新人学习使用；

#### 软件架构
软件架构说明

**flink-examples** 

1.  connectors（中件间连接器示例模块）
2.  examples (模拟电商订单数据并推送到kafka中，以及flink核心流处理客户端)
3.  stream（数据流与算子、方法、窗口等示例代码）
4.  tableapi（table&sql与中件间的使用示例代码）
5.  web（获取flink算子计算后的存储结果，提供给前端展示）
6.  html（前端源码）

后端

1. jdk 1.8
2. flink-1.11.1
3. springboot 2.3.4.RELEASE
4. redis 3.x

前端
1. vue2.x + element.2.x

#### 前端教程

1.  npm install
2.  npm run dev

#### 使用说明

1.  安装Flink 参见 [linux 安装 flink 1.11.1](https://my.oschina.net/u/437309/blog/4961818)
2.  Flink系列文章资源 参见 [Flink 系例](https://my.oschina.net/u/437309?tab=newest&catalogId=7091935)
3.  Flink项目实战 参见[Flink 系例 之 电商项目实战(示例)](https://my.oschina.net/u/437309/blog/4973780)

#### 运行效果
![Flink平台](https://oscimg.oschina.net/oscnet/up-e11782f37acf4fb446f56efc85ea2e0d2f8.JPEG "Flink平台")
（Flink平台）

![大屏监控](https://oscimg.oschina.net/oscnet/up-a147153ca86c23a6dce731ef04cefb65b1e.gif "大屏监控")
（大屏监控）

#### 其它

1.  源码开放下载与使用，如有问题敬请留言与Issues。谢谢！


