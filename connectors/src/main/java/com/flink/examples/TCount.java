package com.flink.examples;

/**
 * @Description 统计表
 * @Author JL
 * @Date 2020/09/19
 * @Version V1.0
 */
public class TCount {

    /**
     * 性别
     */
    private Integer sex;
    /**
     * 数量
     */
    private Integer num;

    public TCount(){}

    public TCount(Integer sex, Integer num){
        this.sex = sex;
        this.num = num;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }
}
