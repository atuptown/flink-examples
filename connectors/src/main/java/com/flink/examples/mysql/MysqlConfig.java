package com.flink.examples.mysql;

/**
 * @Description Mysql数据库连接配置
 * @Author JL
 * @Date 2020/09/17
 * @Version V1.0
 */
public class MysqlConfig {

    public final static String DRIVER_CLASS="com.mysql.jdbc.Driver";

    public final static String SOURCE_DRIVER_URL="jdbc:mysql://127.0.0.1:3306/flink?useUnicode=true&characterEncoding=utf-8&useSSL=false";
    public final static String SOURCE_USER="root";
    public final static String SOURCE_PASSWORD="123456";

}
